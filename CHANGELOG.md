## 4.3.0 (2019.12.21)

* still refactoring (1a027b0) 
* composer update to be 7.1 compliant (231214c) 
* some major refactor (bfc354e) 
* still refactoring (1a027b0) 
* another code cleanup (7120804) 
* composer update to be 7.1 compliant (231214c) 

## 4.2.0 (2019.12.20)


## New features
* some small refactor (7019a36) 
## Documentation
* unit tests mocks  for build (8e8dced) 
## Bug fixes
* some small refactor (7019a36) 

## 4.1.0 (2019.12.18)


## New features
* codeception fake unit for build (b7b68d7) 
* Update .gitlab-ci.yml to run tests (86ea851) 
## Documentation
* update .gitlab-ci.yml (cc4740e) 
* removing pipeline (19916a7) 
## Bug fixes
* removing codeception tests (58ea007) 

### 4.0.4
## Documentation
* readme about widnows machine (fe76f0c) 

### 4.0.3
## Documentation
* readme info about scripts (72a2f86) 

### 4.0.2
## Documentation
* readme more bugs (8afceb7) 

### 4.0.1
## Documentation
* changelog fixes (9e057b2) 

# 4.0.0
## Bug fixes
* remove var_dump from catch (9313da8) 
* handling primitive exceptions (02b9216) 
* Fixing merge and first branch error (1e9e0e4) 
## Documentation
* added readme (4e66503) 
* Changelog generated for 2.0.14 (5e06bfc) 
* remove plugin info (58b69bb) 
* Changes with null branch and merge (d389811) 

# 3.0.0
## New features
* fixed problem with no branch seperator (5948e24) 

### 2.0.14 (2019-12-14)


## Documentation

* remove plugin info (58b69bb) 

### 2.0.13 (2019-12-14)


## Bug fixes

* remove var_dump from catch (9313da8) 

### 2.0.12 (2019-12-14)


## Bug fixes

* handling primitive exceptions (02b9216) 

### 2.0.10 (2019-12-14)


## Bug fixes

* Fixing merge and first branch error (1e9e0e4) 
## Documentation

* Changes with null branch and merge (d389811) 

### 2.0.9
## Bug fixes
* composer.json fix (01cbf29) 

### 2.0.8
## Documentation
* readme fixes (528edc7) 

### 2.0.7
## Documentation
* readme fixes (68d12b1) 

### 2.0.6
## Documentation
* readme fixes (a10d17d) 

### 2.0.5
## Documentation
* readme instructions (f65eccd) 

### 2.0.4
## Documentation
* removing old reamde.MD (4d0b155) 

### 2.0.3
## Documentation
* changeloggen bumping version (c961831) 

### 2.0.2
## Documentation
* changelog fix (02b7235) 

### 2.0.1
## Bug fixes
* no version tag fix (18cbe07) 

# 2.0.0
## New features
* Description automatically fetched (435db0e) 
* Regex for conventionalCommit change  BREAKING CHANGE (98f2b61) 
* Jakis opis  BREAKING CHANGE (d96a4ec) 
* Automating scope and commit type (ee6f340) 
* Scope is fetched from current branch (80827f3) 
## Bug fixes
* composer.json removed some dep. (0da2597) 
* Breaking change selector based on config (46b9d1d) 
## Documentation
* checking (4134cb3) 
* changed readme (71f1fec) 
* fixed old changelog.md (194fa17) 
* removed usless readme info. (3390d9b) 
* Changelog generated for 7.0.0 (892cae2) 
* Changelog fix (1d7eb3c) 
* Changelog generated for 6.0.2 (2793505) 
* Changelog generated (e7908eb) 
* Docs changelog (4b168f4) 
* Version bump (5fae4df) 
