<?php

namespace SyncHot\ConventionalCommit\Exceptions;

class GitException extends \Exception
{
    /**
     * @param string $message
     * @param integer $code
     */
    public function __construct($message = "", $code = 0)
    {
        parent::__construct($message, $code);
    }
}
