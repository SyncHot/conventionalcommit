<?php

namespace SyncHot\ConventionalCommit\Exceptions;

class FileNotFoundException extends \Exception
{
    /**
     * @param string $message
     * @param integer $code
     */
    public function __construct($message = "", $code = 0)
    {
        parent::__construct($message, $code);
    }
}
