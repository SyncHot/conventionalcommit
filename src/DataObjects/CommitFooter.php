<?php
namespace SyncHot\ConventionalCommit\DataObjects;

class CommitFooter {
    
    private $footer;

    public function __construct(?string $footer)
    {
        if(isset($footer)){
            $this->setFooter($footer);
        }
    }

    /**
     * @param string $footer
     * @return void
     */
    public function setFooter(string $footer){
        $this->footer= $footer;
    }

    /**     
     * @return void
     */
    public function getFooter(){
        return $this->footer;
    }
    
    public function __toString()
    {
        return $this->getFooter();
    }
}