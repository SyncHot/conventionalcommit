<?php
namespace SyncHot\ConventionalCommit\DataObjects;

class CommitBody
{

    private $body;

    public function __construct(?string $body)
    {
        if (isset($body)) {
            $this->setBody($body);
        }
    }

    public function setBody(string $body)
    {
        $this->body = $body;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function __toString()
    {
        //Conventional commit required  blank space
        return ' ' . $this->getBody();
    }

}
