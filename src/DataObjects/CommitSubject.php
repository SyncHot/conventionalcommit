<?php
namespace SyncHot\ConventionalCommit\DataObjects;

use SyncHot\ConventionalCommit\DataObjects\Enums\CommitType;

class CommitSubject {
    
    private $type;
    private $scope;
    private $description;
    /**
     * @param CommitType $type
     * @param string $scope
     * @param string $description
     */
    public function __construct(CommitType $type, string $scope, string $description){
        $this->setType($type);
        $this->setScope($scope);
        $this->setDescription($description);
    }

    public function setType(CommitType $type)
    {
        $this->type = $type;
    }

    public function setScope(string $scope)
    {
        $this->scope = $scope;
    }

    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function getType(): string
    {
        return $this->type->getValue();
    }

    public function getScope(): string{
        return $this->scope;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function __toString()
    {
        $output = $this->getType();

        if($this->getScope()){
            $output .= '(' . $this->getScope() . ')';
        }
        
        $output .= ': ';
        $output .= $this->getDescription();
        
        return $output;
    }
}