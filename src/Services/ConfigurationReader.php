<?php

namespace SyncHot\ConventionalCommit\Services;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * 
 * @package SyncHot\ConventionalCommit\Services
 */
class ConfigurationReader
{
    private static $pathToUserConfiguration = __DIR__ . '/../../../../../config/packages/conventionalcommit.yaml';
    private static $pathToLocalConfiguration = __DIR__ . '/../../config/conventionalcommit.yaml';


    /**
     * @return array 
     * @throws ParseException 
     */
    public static function read(): array
    {
        if (self::fileExists(self::$pathToUserConfiguration)) {
            return Yaml::parseFile(self::$pathToUserConfiguration);
        }
        return Yaml::parseFile(self::$pathToLocalConfiguration);
    }

    /**
     * @param string $filePath 
     * @return bool 
     */
    private static function fileExists(string $filePath): bool
    {
        return \file_exists($filePath);
    }
}
