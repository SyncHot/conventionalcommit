<?php

namespace SyncHot\ConventionalCommit\Services;

class ComposerBuild
{
    /**
     * @return void 
     */
    public static function copyHooks()
    {
        $to = __DIR__ . '/../../../../../.git/hooks/';
        $from = __DIR__ . '/../../resources/*';
        exec("cp -Rf $from $to");
        chmod("$to/commit-msg", 0777);
    }
}
