<?php

namespace SyncHot\ConventionalCommit\Command;

use Symfony\Component\Console\Command\Command;
use SyncHot\ConventionalCommit\Command\Commit;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SyncHot\ConventionalCommit\Command\CommitMessageBuilder;

class ConventionalCommit extends Command
{
    protected static $defaultName = 'commit:check';
    private const COMMIT_SUCCESSFUL = 'Commit was successful';
    public const COMMIT_MESSAGE_FILE_LOCATION = 'gitCommitMessageLocation';

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription('Commit message validation.')
            ->addArgument(self::COMMIT_MESSAGE_FILE_LOCATION, InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input 
     * @param OutputInterface $output 
     * @return int 
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
       
        $commitMessage = Commit::getConventionalCommit($input);
        
        if(!Commit::isValidMessage($commitMessage)){
            try{
                Commit::createConventionalCommit(
                    CommitMessageBuilder::build($io,  $commitMessage), 
                    $input
                );
            }catch(\Exception $ex){
                $io->error($ex->getMessage());
                return 1;
            }
        }

        $io->success(self::COMMIT_SUCCESSFUL);
        return 0;
    }
}
