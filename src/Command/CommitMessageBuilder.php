<?php

namespace SyncHot\ConventionalCommit\Command;

use Symfony\Component\Console\Style\SymfonyStyle;
use SyncHot\ConventionalCommit\Services\Validator;
use SyncHot\ConventionalCommit\DataObjects\CommitBody;
use SyncHot\ConventionalCommit\Exceptions\GitException;
use SyncHot\ConventionalCommit\DataObjects\CommitFooter;
use SyncHot\ConventionalCommit\DataObjects\CommitMessage;
use SyncHot\ConventionalCommit\DataObjects\CommitSubject;
use SyncHot\ConventionalCommit\DataObjects\Enums\CommitType;
use SyncHot\ConventionalCommit\Services\ConfigurationReader;

class CommitMessageBuilder
{
    private const BREAKING_CHANGE = 'BREAKING CHANGE: ';
    private const COMMIT_FIELDS = [
        'type', 'scope', 'description', 'body', 'footer',
    ];

    public static function build(SymfonyStyle $io, string $inputCommitMessage): CommitMessage
    {

        $commit = [];

        $io->title('Welcome to DALBAYOB mode');

        $breakingChange = self::breakingChangeSelectorActive($io);

        foreach (self::COMMIT_FIELDS as $commitField) {

            $config = ConfigurationReader::read()[$commitField];
            $commitFieldDefault = self::getDefaultValue($commit, $commitField, $breakingChange, $inputCommitMessage);

            if (!empty($config['values'])) {
                $commit[$commitField] = $io->choice(
                    $config['message'],
                    $config['values'],
                    $commitFieldDefault
                );
            } else {
                $commit[$commitField] = $io->ask(
                    $config['message'],
                    $commitFieldDefault,
                    function ($someValue) use ($commitField) {
                        Validator::validate($commitField, $someValue);
                        return $someValue;
                    }
                );
            }
        }

        return new CommitMessage(
            new CommitSubject(new CommitType($commit['type']), $commit['scope'], $commit['description']),
            new CommitBody($commit['body']),
            new CommitFooter($commit['footer'])
        );
    }
    
    /**
     * @return string|null
     */
    private static function getCurrentBranchName(): ?string
    {
        try {
            return `git rev-parse --abbrev-ref HEAD 2>/dev/null`;
        } catch (\Exception $ex) {
            throw new GitException($ex->getMessage, $ex->getCode());
        }
    }

    /**
     * @param string $defaultValue 
     * @return string 
     */
    private static function replaceDefaults(string $defaultValue): string
    {
        $output = trim(preg_replace("/{currentBranchName}/", self::getCurrentBranchName(), $defaultValue));
        $output = trim(preg_replace("/{scopeBasedOnBranchName}/", self::getCurrentScope(), $output));
        return  trim(preg_replace("/{commitTypeBasedOnBranchName}/", self::getCurrentCommitType(), $output));
    }
    /**
     * @return string 
     */
    private static function explodeBranchName(): array
    {
        return preg_split("/\//", self::getCurrentBranchName());
    }

    /**
     * @return string 
     */
    private static function getCurrentCommitType(): string
    {
        $branchName = self::explodeBranchName();

        if (isset($branchName[1])) {
            return self::mapBranchNameToScope($branchName[0]);
        }
        return "feat";
    }

    /**
     * @return string 
     */
    private static function getCurrentScope(): string
    {
        $branchName = self::explodeBranchName();

        if (!empty($branchName[1])) {
            return $branchName[1];
        }
        return "";
    }

    /**
     * @param mixed $branchName 
     * @return string 
     */
    private static function mapBranchNameToScope($branchName): string
    {
        $map = ConfigurationReader::read()['mapBranchestoType'];

        return array_key_exists($branchName, $map) ? $map[$branchName] : "";
    }

    /**
     * @param mixed $io 
     * @return string 
     */
    private static function breakingChangeSelectorActive($io): string
    {

        if (ConfigurationReader::read()['breakingChangeSelectorActive']) {
            return  $io->confirm("Is the commit a breaking change?", false);
        }
    }

    /**
     * @param array $commit 
     * @param string $field 
     * @param mixed $breakingChange 
     * @param string $inputCommitMessage 
     * @return string 
     */
    private static function getDefaultValue(array $commit, string $field, $breakingChange, $inputCommitMessage = ""): string
    {
        if ($field === 'description' && !empty($inputCommitMessage)) {
            return $inputCommitMessage;
        }

        if ($breakingChange && $field === 'footer') {
            return self::BREAKING_CHANGE . $commit['description'];
        }
        return self::replaceDefaults(ConfigurationReader::read()[$field]['default']);
    }
}
